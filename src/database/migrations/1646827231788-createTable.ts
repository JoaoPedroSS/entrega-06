import {MigrationInterface, QueryRunner} from "typeorm";

export class createTable1646827231788 implements MigrationInterface {
    name = 'createTable1646827231788'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "cart" DROP CONSTRAINT "FK_588be05d18b7e4427a06e219528"`);
        await queryRunner.query(`ALTER TABLE "cart" RENAME COLUMN "usersId" TO "userId"`);
        await queryRunner.query(`ALTER TABLE "cart" RENAME CONSTRAINT "REL_588be05d18b7e4427a06e21952" TO "UQ_756f53ab9466eb52a52619ee019"`);
        await queryRunner.query(`ALTER TABLE "cart" ADD CONSTRAINT "FK_756f53ab9466eb52a52619ee019" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "cart" DROP CONSTRAINT "FK_756f53ab9466eb52a52619ee019"`);
        await queryRunner.query(`ALTER TABLE "cart" RENAME CONSTRAINT "UQ_756f53ab9466eb52a52619ee019" TO "REL_588be05d18b7e4427a06e21952"`);
        await queryRunner.query(`ALTER TABLE "cart" RENAME COLUMN "userId" TO "usersId"`);
        await queryRunner.query(`ALTER TABLE "cart" ADD CONSTRAINT "FK_588be05d18b7e4427a06e219528" FOREIGN KEY ("usersId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
