import jwt from 'jsonwebtoken'
import * as bcrypt from 'bcrypt'
import { getCustomRepository } from 'typeorm'
import UsersRepository from '../repositories/userRepositories'
export const resetPasswordService = async (
  token: string,
  newPassword: string
): Promise<any> => {
  const userRepository = getCustomRepository(UsersRepository)
  const result = jwt.verify(
    token,
    process.env.SECRET_KEY as string,
    async (err: any, decoded: any) => {
      if (err) {
        return undefined
      }
      const hashedPassword = bcrypt.hashSync(newPassword, 10)
      const data = {
        password: hashedPassword
      }
      await userRepository.update(decoded.uuid, data)
      return 'Password reseted'
    }
  )

  return result
}
