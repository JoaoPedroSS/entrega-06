import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { getCustomRepository, getRepository } from "typeorm";
import { Users } from "../entities/Users";
// import { ErrorHandler, handleError } from "../error/error";
import UsersRepository from "../repositories/userRepositories";

export const authenticateUser = async (email: string, password: string): Promise<any> => {
  const userRepository = getCustomRepository(UsersRepository);

  const user = await userRepository.findByEmail(email);
  
    if (user === undefined || !bcrypt.compareSync(password, user.password)) {
      return undefined
    }

  const token = jwt.sign({ id: user.id}, process.env.SECRET_KEY as string, {
      expiresIn: "1d",
  });

  return token;
};
