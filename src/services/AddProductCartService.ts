import { getCustomRepository, getRepository } from "typeorm";
import { Cart } from "../entities/Cart";
import { Product } from "../entities/Products";
import { Users } from "../entities/Users";
import CartRepository from "../repositories/carUserRepositories";
import UsersRepository from "../repositories/userRepositories";

interface ProductCartProps {
    product_id: string;
    id_user: string;
}


export class AddProductCart {
    async execute({product_id, id_user}: ProductCartProps):Promise<Product | Error>{
        const repo = getRepository(Product)
        const repoUser = getCustomRepository(UsersRepository)
        const cartRepo = getRepository(Cart)
        const userFound = await repoUser.findOne(id_user)
        
        console.log(id_user)
        //const cartFound = await cartRepo.findOne({"userId": id_user})
        
        
        
        //console.log(cartFound)
        // console.log(cartFound)

        const productFound = await repo.findOne(product_id)
        
        //console.log(productFound.id)

        // if (!productFound) {
        //     return new ErrorHandler(400, "Product not found");
        // }

        const cartUser = cartRepo.create({ "products": [productFound]})
        
        await cartRepo.save(cartUser)

       // console.log(cartUser.products)

        return productFound

    }
}