import { getRepository } from "typeorm"
import { Product } from "../entities/Products"


export const GetAllProductIdService = async (id: string) => {

    const repo = getRepository(Product);

    const productId = await repo.findOne(id);

    if(!productId) {
        throw new Error("Product does not exists!")
    }

    return productId;
}