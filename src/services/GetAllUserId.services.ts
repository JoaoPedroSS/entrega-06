import { Any, getRepository } from "typeorm";
import { Users } from "../entities/Users";



export class GetAllUserId {
   async execute ({id}): Promise<any | Error> {
    const repo = getRepository(Users);

    const userFound = await repo.findOne(id);

    if(!userFound){
      return new Error("Id not found!")
    }

    if (userFound.isAdm === false) {
      return userFound
    }

    if (userFound.isAdm === true) {
      return await repo.find();
    }

  };
}
