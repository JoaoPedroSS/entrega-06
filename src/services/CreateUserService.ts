import { getRepository } from "typeorm";
import { Cart } from "../entities/Cart";
import { Users } from "../entities/Users";



interface UserProps {
    name: string;
    email: string;
    password: string;
    isAdm: boolean;
}

export class CreateUser {
  async execute ({name, email, password, isAdm}: UserProps): Promise<Users | Error> {
  const repo = getRepository(Users);
  const repoCart = getRepository(Cart);

  

  if(await repo.findOne({email})) {
    return new Error("Email alaready exists!")
  }
  let newUser = repo.create({
    name,
    email,
    password,
    isAdm
  });


  
  
  await repo.save(newUser);
  
  const teste = newUser

  let cartUser = repoCart.create({"user": teste})

  await repoCart.save(cartUser)


  return newUser;

  }
}
