import { getCustomRepository, getRepository } from "typeorm";
import { Product } from "../entities/Products";
import ProductRepository from "../repositories/productRepository";

interface ProductProps {
  name: string;
  price: number;
  quantity: number;
  description: string;
}

export class CreateProduct {
  async execute({
    name,
    price,
    quantity,
    description,
  }: ProductProps): Promise<Product | Error> {
    const repo = getRepository(Product);

    if (await repo.findOne({ name })) {
      return new Error("Product already exists!");
    }

    const newProduct = repo.create({
      name,
      price,
      quantity,
      description,
    });

    await repo.save(newProduct);

    return newProduct;
  }
}
