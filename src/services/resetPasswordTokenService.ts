import jwt from 'jsonwebtoken'
import { getCustomRepository } from 'typeorm'
import UsersRepository from '../repositories/userRepositories'

export const resetPasswordTokenService = async (
  email: string
): Promise<any | undefined> => {
  const userRepository = getCustomRepository(UsersRepository)
  const user = await userRepository.findOne({ email })
  if (!user) {
    return undefined
  }
  const token = jwt.sign({ uuid: user.id }, process.env.SECRET_KEY as string, {
    expiresIn: '10m'
  })
  return token
}
