import { getRepository } from "typeorm";
import { Users } from "../entities/Users";

export const GetAllUser = async () => {
  
  const repo = getRepository(Users);

  const users = await repo.find();

  return users;
};
