import { Any, getRepository } from "typeorm";
import { Cart } from "../entities/Cart";
import { Users } from "../entities/Users";



export const GetAllCart = async () => {
  
    const repo = getRepository(Cart);
  
    const carts = await repo.find({
        relations: ['products']
    });
  
    return carts;
  };
  
