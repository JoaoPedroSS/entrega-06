import { getRepository } from "typeorm"
import { Product } from "../entities/Products"




export const GetAllProductService = async () => {

    const repo = getRepository(Product);

    const productId = await repo.find();

    if(!productId) {
        throw new Error("Product does not exists!")
    }

    return productId;
}