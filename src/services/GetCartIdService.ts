import { Any, getRepository } from "typeorm";
import { Cart } from "../entities/Cart";
import { Users } from "../entities/Users";



export class GetCartId {
   async execute ({id}): Promise<any | Error> {
    const repo = getRepository(Cart);

    const cartFound = await repo.findOne({id}, {
        relations: ['products']
    });

    if(!cartFound){
      return new Error("Id not found!")
    }

    return cartFound

  };
}
