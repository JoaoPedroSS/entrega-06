import { Router } from "express";
import { CreateUserController } from "./controllers/CreateUserController";
import { listUser } from "./controllers/GetAllUser.controller";
import { listUserId } from "./controllers/GetAllUserId.controller";
import { loginUser } from "./controllers/LoginUser.controller";
import { isAdm } from "./middleware/userIsAdm";
import { isAuthenticatedUser } from "./middleware/isAuthenticate";
import { CreateProductController } from "./controllers/CreateProduct.controller";
import { GetAllProductIdController } from "./controllers/GetAllProductIdController";
import { GetAllProductController } from "./controllers/GetAllProductController";
import { AddProductCartController } from "./controllers/AddProductCartController";
import { ResetPasswordTokenController } from "./controllers/resetPasswordTokenController";
import { ResetPasswordController } from "./controllers/resetPasswordController";
import { GetCartController } from "./controllers/GetCartController";
import { listCartId } from "./controllers/GetCartIdController";
import { DeleteProductCartController } from "./controllers/deleteProductCartCOntroller";

const routes = Router();


routes.post("/user", new CreateUserController().handle)
routes.get('/user/:id', isAuthenticatedUser,new listUserId().handle )
routes.get('/user', isAuthenticatedUser, isAdm , listUser)
routes.post('/login', loginUser)

routes.get('/cart', GetCartController)
routes.get('/cart/:id', new listCartId().handle)
routes.delete('/cart/:id', isAuthenticatedUser,new DeleteProductCartController().handle)

routes.post("/product", isAuthenticatedUser, new CreateProductController().handle)
routes.get('/product/:id', GetAllProductIdController)
routes.get('/product', GetAllProductController)

routes.post('/cart', isAuthenticatedUser, new AddProductCartController().handle)

routes.post('/recuperar', new ResetPasswordTokenController().execute)
routes.post('/alterar_senha	', new ResetPasswordController().execute)




export { routes };