import { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";
import { Users } from "../entities/Users";
import { getRepository } from "typeorm";
import { ErrorHandler } from "../error/error";


export const isAdm = (request: Request, response: Response, next: NextFunction) => {

    const tokenUser = request.headers.authorization.split(' ')[1];

    jwt.verify(tokenUser as string, process.env.SECRET_KEY as string, async (err: any, decoded: any) => {
        
        const user = getRepository(Users);

        const idUser = decoded.id;

        request.user = { id: idUser};

        const userFound = await user.findOne(idUser);

        if(userFound.isAdm === false) {
            return next(new ErrorHandler(400, "User is not Admin!"));
        }

        next();
    })
}