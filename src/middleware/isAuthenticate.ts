import { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";

export const isAuthenticatedUser = async (request: Request, response: Response, next: NextFunction) => {

    const token = request.headers.authorization?.split(' ')[1];

    if (!token) {
        return response.status(401).json({ message: 'Missing authorization headers' })
      
    }

    jwt.verify(token as string, process.env.SECRET_KEY as string, (err: any, decoded: any) => {
        if(err) {
            return next(new Error('Invalid or expired token'));
        }

        const userId = decoded.id;

        request.user = { id: userId };

        next();
    });

}