import "reflect-metadata";
import express from "express";
import "./database";
import { routes } from "./routes";
import swaggerUiExpress from "swagger-ui-express";
const app = express();


app.use(express.json());

// app.use("/api-doc", swaggerUiExpress.serve, swaggerUiExpress.setup(swaggerDocument))
app.use(routes);



app.listen(process.env.PORT || 3001, () => console.log("Server is running"!))