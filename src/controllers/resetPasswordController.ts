import { Request, Response } from 'express'
import { resetPasswordService } from '../services/resetPasswordService'

export class ResetPasswordController {
  async execute (req: Request, res: Response): Promise<Response> {
    try {
      const token = req.query.token
      console.log(token)
      if (!token) {
        return res.status(401).json({ message: 'Missing code' })
      }
      const { newPassword } = req.body
      if (!newPassword) {
        return res.status(400).json({ message: 'Missing param: newPassword' })
      }
      const response = await resetPasswordService(token as string, newPassword)
      if (!response) {
        return res.status(401).json({ message: 'Invalid or expired token' })
      }
      return res.status(200).json({ message: response })
    } catch (error: any) {
      return res.status(500).json({
        message: error.message
      })
    }
  }
}
