import { NextFunction, Request, Response } from "express";
import { GetAllProductService } from "../services/GetAllProductService";

export const GetAllProductController = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {

    try {
        const product = await  GetAllProductService();
        return response.send(product);
      } catch (err) {
        next(err);
      }
};
