import { Request, Response } from "express";
import { GetCartId } from "../services/GetCartIdService";

export class listCartId {
    async handle(request: Request, response: Response): Promise<any> {
        const { id } = request.params;
        
        const service = new GetCartId();

        const result = await service.execute({id})


        if(result instanceof Error){
          return response.status(400).json({message: result.message})
        }

        return response.json(result)
    }
}
