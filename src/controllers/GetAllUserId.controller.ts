import { NextFunction, Request, Response } from "express";
import { GetAllUserId } from "../services/GetAllUserId.services";


export class listUserId {
    async handle(request: Request, response: Response): Promise<any> {
        const { id } = request.params;
        
        const service = new GetAllUserId();

        const result = await service.execute({id})


        if(result instanceof Error){
          return response.status(400).json({message: result.message})
        }

        return response.json(result)
    }
}

