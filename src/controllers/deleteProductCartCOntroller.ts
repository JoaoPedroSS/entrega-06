import { Request, Response } from "express";
import { DeleteProductCartService } from "../services/DeleteCartProductService";


export class DeleteProductCartController {
    async handle(request: Request, response: Response) {
        const { id } = request.params;

        const user = request.user.id

        const service = new DeleteProductCartService();

        const result = await service.execute(id, user);

        if(result instanceof Error) {
            return response.status(400).json({message: result.message});
        }

        return response.status(204).end();

    }
}