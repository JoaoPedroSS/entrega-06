import { Request, Response } from "express";
import { AddProductCart } from "../services/AddProductCartService";


export class AddProductCartController {
    async handle(request: Request, response: Response): Promise<any> {

        const { product_id } = request.body;

        const id_user = request.user.id

        const service = new AddProductCart()

        const result = await service.execute({
            product_id, id_user
        });

        if(result instanceof Error) {
            return response.status(400).json({message: result.message})
        }
    
        return response.json(result)
    }
}