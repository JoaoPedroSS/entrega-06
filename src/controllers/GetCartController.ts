import { NextFunction, Request, Response } from "express";
import { GetAllCart } from "../services/GetCartService";

export const GetCartController = async (request: Request, response: Response, next: NextFunction) => {
    try {
        const cart = await  GetAllCart();
        return response.send(cart);
      } catch (err) {
        next(err);
      }
} 