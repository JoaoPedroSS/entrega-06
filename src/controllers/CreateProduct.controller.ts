import { NextFunction, Request, Response } from "express";
import { CreateProduct } from "../services/CreateProductService";

export class CreateProductController {
  async handle(request: Request, response: Response): Promise<any> {
    const { name, price, quantity, description } = request.body;

    const service = new CreateProduct();

    const result = await service.execute({
      name,
      price,
      quantity,
      description,
    });

    if(result instanceof Error) {
        return response.status(400).json({message: result.message})
    }

    return response.json(result)
  }
}
