import { NextFunction, Request, Response } from "express";
import { GetAllProductIdService } from "../services/GetAllProductIdService";
export const GetAllProductIdController = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
    const { id } = request.params;

    try {
        const product = await  GetAllProductIdService(id);
        return response.send(product);
      } catch (err) {
        next(err);
      }
};
