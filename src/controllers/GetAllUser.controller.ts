import { NextFunction, Request, Response } from "express";
import { GetAllUser } from "../services/GetAllUserService";


export const listUser = async (request: Request, response: Response, next: NextFunction) => {
    try {
        const user = await  GetAllUser();
        return response.send(user);
      } catch (err) {
        next(err);
      }
} 