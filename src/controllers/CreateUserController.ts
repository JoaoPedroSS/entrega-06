import { NextFunction, Request, Response } from "express";
import { CreateUser } from "../services/CreateUserService";

export class CreateUserController {
  async handle(request: Request, response: Response): Promise<any> {
    const { name, email, password, isAdm } = request.body;

    const service = new CreateUser();

    const result = await service.execute({
      name,
      email,
      password,
      isAdm,
    });

    if (result instanceof Error) {
      return response.status(400).json({ message: result.message });
    }
    return response.json(result);
  }
}
