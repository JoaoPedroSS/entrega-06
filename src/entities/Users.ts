import { Column, PrimaryColumn, Entity, BeforeInsert, PrimaryGeneratedColumn } from "typeorm";
import { v4 as uuid } from "uuid";
import bcrypt from "bcrypt";

@Entity("user")
export class Users {

    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    name: string;

    @Column()
    email: string;

    @Column()
    password: string;

    @Column()
    isAdm: boolean;

    @BeforeInsert()
    hashPassword() {
        this.password = bcrypt.hashSync(this.password, 10);
    }

}