import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, ManyToMany, JoinTable} from 'typeorm';
import { Product } from './Products';
import { Users} from './Users';


@Entity()
export class Cart {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @OneToOne(type => Users) @JoinColumn()
    user: Users;

    @ManyToMany(type => Product) @JoinTable()
    products: Product[];
}