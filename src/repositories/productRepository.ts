import { EntityRepository, Repository } from "typeorm";
import { Product } from "../entities/Products";

@EntityRepository(Product)
class ProductRepository extends Repository<Product> {
  public async findByName(name: string): Promise<Product | undefined> {
    const user = await this.findOne({
      where: {
        name,
      },
    });

    return user;
  }
}

export default ProductRepository;