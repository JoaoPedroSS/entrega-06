import { EntityRepository, Repository } from "typeorm";
import { Product } from "../entities/Products";

@EntityRepository(Product)
class CartRepository extends Repository<Product> {
  public async findByCart(user: string): Promise<Product | undefined> {
    const userId = await this.findOne({
      where: {
        user
      },
    });

    return userId;
  }
}

export default CartRepository;